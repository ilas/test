/**
 * Created by root on 12.08.2015.
 */

function Spandl ( rc, mode ){

    //this.cnt =
    this.rc = rc
    this.mode = mode
    this.workers = {'listen':'listen', 'gen': 'generator', 'getErrors':'clearError' }
    this.myname = this.seyMyName()
    console.log('current mode - ' + mode )
}


Spandl.prototype.generator = function(){

    this.updateSpeakerStatus();
    var c = this.rc
    c.set("new:message:"+ Date.now() , this.getMessage() );
    var _self = this
    setTimeout( function(){ _self.run() }, 500);
}

Spandl.prototype.getMessage = function(){

    this.cnt = this.cnt || 0;
    return this.cnt++;

}


Spandl.prototype.listen= function(){

    var c  = this.rc
    var _self = this
    var async = require('async')

    c.keys('new*', function(err,data) {

        if( data.length == 0)
            _self.takeSpeaking()
        else
        async.each(data, function( key, callback ){

            c.watch( key )
                var multi = c.multi()
                var new_key_name = key.replace('new','h')
                multi.rename(key, new_key_name)
                multi.exec(function (err, replies) {
                    if( replies === null) {
                       callback("transaction aborted!")
                    }
                    else
                    {
                        _self.eventHandler( new_key_name ,
                            function(err, msg){ _self.eventHandlerCallback(err, msg) }
                            )
                        callback()
                    }
                });


            }, function(){
                   setTimeout( function(){ _self.run() }, 1000);
            })

    });

}

Spandl.prototype.run = function(){
    this[ this.workers[ this.mode ] ]()
}

//Приложение, получая сообщение, передаёт его в данную функцию:

Spandl.prototype.eventHandler =  function(msg, callback){

            function onComplete(){

            var error = Math.random() > 0.85;
            callback(error, msg);

        }

// processing takes time...

        setTimeout(onComplete, Math.floor(Math.random()*1000));

    }

Spandl.prototype.eventHandlerCallback =  function(err, msg){
   if(err){
       var c = this.rc
       c.get( msg , function (err, data) {
           c.hset(["error_messages", msg, data], function(){});
           c.del( msg )
       });

   }

}

Spandl.prototype.seyMyName = function(){
    var crypto = require('crypto');
    var name = process.pid + Date.now() + "" + Math.random(10000)
    var hash = crypto.createHash('md5').update(name.toString()).digest('hex');
    return hash
}

Spandl.prototype.updateSpeakerStatus = function(){

    this.rc.set("speaker_status", this.myname + ":" + Date.now() )

}

Spandl.prototype.takeSpeaking = function(){

    var c = this.rc
    var  _self = this
    var myname = this.myname
    c.watch( 'speaker_status' )

    c.get("speaker_status", function (err, obj) {

        if(obj === null){
            var status = ['', 0]
        }else
        var status = obj.split(':')

        if( ( (Date.now() - status[ 1 ] ) > 1000 ) )
        {
            console.log( 'try become  a speaker' )


            var multi = c.multi()

            multi.set("speaker_status", this.myname + ":" + Date.now() )
            multi.exec(function (err, replies) {


                console.log( replies )
                if( replies === null) {

                    console.log('speaker already speaking')

                }
                else{
                    _self.mode = 'gen'
                    console.log('current mode - ' +  _self.mode )
                }

                _self.run()
            });

        }
        else
            setTimeout( function(){ _self.run() }, 1000);

    });




}

Spandl.prototype.clearError = function(){

    var c = this.rc
    var async = require("async")

    c.hkeys("error_messages", function (err, replies) {
        console.log(replies.length + " errors:");

        async.each(replies, function (key, callback) {

            c.hget("error_messages",key, function(err, data){
               console.log(key+ " - " + data)
            })

            c.hdel("error_messages", key, function(){});
            callback()


        }, function () {
           c.quit();
        });
    });

}


module.exports = Spandl